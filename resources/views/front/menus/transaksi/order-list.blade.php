@extends('front.layouts.default')
@section('title', 'Profile')
@section('content')
<div class="main-wrapper">
  <div class="main">
    <div class="main-inner">
      <div class="page-title">
        <div class="container">
          <h1>Users</h1>
        </div>
      </div>
      <div class="container">
        <nav class="breadcrumb">
          <a class="breadcrumb-item" href="{{url('/')}}">Home</a>
          <a class="breadcrumb-item" href="javascript:void(0);">Profile</a>
          <span class="breadcrumb-item active">List</span>
        </nav>
        <div class="table-wrapper">
          <table id="tabledata" class="table table-bordered">
            <thead>
              <tr>
                <th class="min-width center">Date</th>
                <th class="min-width center">Grandtotal</th>
                </tr>
            </thead>
            <tbody>
              @foreach($orderlist as $value)
              <tr>
                <td class="min-width center id">{{$value->date}}</td>
                <td class="min-width center id">{{$value->grandtotal}}</td>
                <td class="min-width center">
                  <div class="btn-group" role="group">
                    <button id="btnGroupDrop1" type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" >
                      Actions
                    </button>
                    <div class="dropdown-menu">
                      <a class="dropdown-item" href="{{url('order/cancel?id='.$value->id)}}">Cancel Order</a>
                    </div>
                  </div>
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>

@push('styles')
<link href="{{asset('assets/libraries/datatables/css/dataTables.bootstrap4.css')}}" rel="stylesheet" type="text/css">
@endpush
@push('scripts')
<script type="text/javascript" src="{{asset('assets/libraries/datatables/js/jquery.dataTables.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/libraries/datatables/js/dataTables.bootstrap4.js')}}"></script>
<script>
	$(function(){
    thisform.init();
	}), thisform = {
    init : function()
    {
      thisform.d_init();
    },
    d_init: function() {
      var t = $("#tabledata");
      t.DataTable();
    },
  }
</script>
@endpush
@endsection
