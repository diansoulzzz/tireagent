@extends('front.layouts.default')
@section('title', 'Bengkel')
@section('content')


<div class="main-wrapper">
  <div class="main">
    <div class="main-inner">
      <div class="listing-hero">
        <div class="listing-hero-inner">
          <div class="container">
            <div class="listing-hero-image" style="background-image: url({{asset(Storage::disk('public')->url($detail->filepath))}})"></div>
            <!-- /.listing-hero-image -->
            <!-- $allbengkel as $bengkel; -->
            <h1>{{$detail->name}}<i class="fa fa-check"></i></h1>
            <address>
              {{$detail->address}}
            </address>

          </div>
          <!-- /.container -->
        </div>
        <!-- /.listing-hero-inner -->
      </div>
      <!-- /.listing-hero -->
      <div class="listing-toolbar-wrapper">
        <div class="listing-toolbar" data-spy="affix" data-offset-top="203">
          <div class="container">
            <ul class="nav">
            </ul>
            <!-- /.nav -->
          </div>
          <!-- /.container -->
        </div>
        <!-- /.listing-toolbar -->
      </div>
      <!-- /.listing-toolbar-wrapper -->
      <div class="container">
        <nav class="breadcrumb">
          <a class="breadcrumb-item" href="javascript:void(0);">Home</a>
          <a class="breadcrumb-item" href="javascript:void(0);">Bengkel</a>
          <span class="breadcrumb-item active">Detail</span>
        </nav>
        <div class="row">
          <div class="col-md-12 col-lg-12">
            <div class="listing-detail-section" id="listing-detail-section-description" data-title="{{$detail->name}}">
              <div class="gallery">
                <div class="gallery-item" style="background-image: url({{asset(Storage::disk('public')->url($detail->filepath))}});">
                </div>
              </div>
            </div>
            <!-- /.listing-detail-section -->
            <!-- /.listing-detail-section -->
            <div class="listing-detail-section" id="listing-detail-section-contact-information" data-title="Contact">
              <h2>Contact Information</h2>
              <div class="box">
                <div class="box-inner">
                  <div class="overview overview-half overview-no-margin">
                    <ul>
                      <li><strong>Owner</strong><span>{{$detail->user->name}}</span></li>
                      <li><strong>Address</strong><span>{{$detail->address}}</span></li>
                      <li><strong>Phone</strong><span>{{$detail->no_telp}}</span></li>
                      <li>
                        <ul class="amenities">
                          <li class="{{($detail->mobility)?'yes':'no'}}">Mobility</li>
                        </ul>
                      </li>
                    </ul>
                  </div>
                  <!-- /.overview -->
                </div>
                <!-- /.box-inner -->
              </div>
              <!-- /.box -->
            </div>
            <!-- /.listing-detail-section -->
            <form method="post" enctype="multipart/form-data" action="{{url()->current()}}" class="mb80">
              {{ csrf_field() }}
              <div class="listing-detail-section" id="listing-detail-section-menu" data-title="Items">
                <h2>Items</h2>
                <table class="table table-bordered">
                  <thead>
                    <tr>
                      <th class="min-width">Check</th>
                      <th class="min-width">Name</th>
                      <th class="min-width center price">Price</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($detail->items as $item)
                    <tr>
                      <td>
                        <input type="checkbox" name="item[]" value="{{$item->id}}" checked>
                      </td>
                      <td>
                        <h2>
                          <span class="inline-block va-middle">{{$item->nama}}</span>
                        </h2>
                      </td>
                      <td class="min-width center price">Rp. {{number_format($item->harga)}}</td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
              <div class="center">
                <button type="submit" class="btn btn-primary">Order</a>
              </div>
            </form>
            <div class="listing-detail-section" id="listing-detail-section-map-position" data-title="Map Position">
              <h2>Map Position</h2>
              <iframe class="mb30" style="height:320px;width:100%;border:0;" src="https://www.google.com/maps/embed/v1/place?q={{$detail->latitude}},{{$detail->longitude}}&amp;key=AIzaSyC9WDzyaDiVoCVWJuASWeLBErxSNQ-30cY">
              </iframe>
            </div>

          </div>
          <!-- /.col-* -->
          <!-- /.col-* -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </div>
    <!-- /.main-inner -->
  </div>
  <!-- /.main -->
</div>
@push('styles')
<link href="{{asset('assets/libraries/dropify/dist/css/dropify.min.css')}}" rel="stylesheet" type="text/css">
@endpush
@push('scripts')
<script type="text/javascript" src="{{asset('assets/libraries/dropify/dist/js/dropify.min.js')}}"></script>
<script>
	$(function(){
    thisform.init();
	}), thisform = {
    init : function()
    {
      thisform.i_init();
      // thisform.p_init();
      // thisform.masked_inputs();
    },
		// p_init : function()
		// {
		// 	var i = $("#validate");
		// 	i.parsley().on("form:submit", function() {
    //     $(".masked_input").inputmask('remove');
		// 	});
		// },
    // masked_inputs: function() {
    //   $maskedInput = $(".masked_input"),
    //   $maskedInput.length && $maskedInput.inputmask()
    // },
    i_init: function()
    {
      $(".dropify").dropify({
        messages: {
          "default": "Choose Image",
          replace: "Replace",
          remove: "Remove",
          error: "Error"
        }
      });
    },
  }
</script>
@endpush

@endsection
