@extends('front.layouts.default')
@section('title', 'Bengkel')
@section('content')


<div class="main-wrapper">
  <div class="main">
    <div class="main-inner">
      <div class="page-title">
        <div class="container">
          <h1>Checkout
          </h1>
          <div class="page-title-actions">
            <div class="switcher">
              <strong>Currency</strong>
              <ul>
                <li class="active"><a href="#">USD</a></li>
                <li><a href="#">EUR</a></li>
              </ul>
            </div>
            <!-- /.switcher -->
            <div class="switcher">
              <strong>Language</strong>
              <ul>
                <li class="active"><a href="#">EN</a></li>
                <li><a href="#">FR</a></li>
                <li><a href="#">DE</a></li>
              </ul>
            </div>
            <!-- /.switcher -->
          </div>
          <!-- /.page-title-actions -->
        </div>
        <!-- /.container-->
      </div>
      <!-- /.page-title -->
      <div class="container">
        <nav class="breadcrumb">
          <a class="breadcrumb-item" href="#">Home</a>
          <a class="breadcrumb-item" href="#">Library</a>
          <a class="breadcrumb-item" href="#">Data</a>
          <span class="breadcrumb-item active">Bootstrap</span>
        </nav>
        <div class="progress-steps">
          <div class="row">
            <div class="col-sm-3 col-md-3">
              <a class="progress-step done" href="#">
                <i class="fa fa-check"></i>
                <span class="title">Request Package</span>
              </a>
            </div>
            <!-- /.col -->
            <div class="col-sm-3 col-md-3">
              <a class="progress-step done" href="#">
                <i class="fa fa-check"></i>
                <span class="title">Select Listings</span>
              </a>
            </div>
            <!-- /.col -->
            <div class="col-sm-3 col-md-3">
              <a class="progress-step active" href="#">
                <i class="fa fa-bell"></i>
                <span class="title">Extras Features</span>
              </a>
            </div>
            <!-- /.col -->
            <div class="col-sm-3 col-md-3">
              <a class="progress-step inactive" href="#">
                <i class="fa fa-plug"></i>
                <span class="title">Checkout</span>
              </a>
            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->
        </div>
        <!-- /.progress-steps -->
        <div class="row">
          <div class="col-md-8 col-lg-9">
            <form method="post" action="#">
              <div class="box">
                <div class="box-inner">
                  <div class="box-title">
                    <h2>Personal Information</h2>
                  </div>
                  <!-- /.box-title -->
                  <div class="row">
                    <div class="col">
                      <div class="form-group">
                        <label>First name</label>
                        <input class="form-control" type="text">
                      </div>
                      <!-- /.form-group -->
                      <div class="form-group">
                        <label>E-mail</label>
                        <input class="form-control" type="text">
                      </div>
                      <!-- /.form-group -->
                    </div>
                    <!-- /.col -->
                    <div class="col">
                      <div class="form-group">
                        <label>Surname</label>
                        <input class="form-control" type="text">
                      </div>
                      <!-- /.form-group -->
                      <div class="form-group">
                        <label>Phone number</label>
                        <input class="form-control" type="text">
                      </div>
                      <!-- /.form-group -->
                    </div>
                    <!-- /.col -->
                  </div>
                  <!-- /.row -->
                </div>
                <!-- /.box-inner -->
              </div>
              <!-- /.box -->
              <div class="box">
                <div class="box-inner">
                  <div class="box-title">
                    <h2>Payment Information</h2>
                  </div>
                  <!-- /.box-title -->
                  <div class="form-group">
                    <ul class="credit-cards">
                      <li><i class="fa fa-cc-stripe"></i></li>
                      <li><i class="fa fa-cc-discover"></i></li>
                      <li><i class="fa fa-cc-paypal"></i></li>
                      <li><i class="fa fa-cc-visa"></i></li>
                    </ul>
                    <!-- /.credit-cards -->
                  </div>
                  <div class="row">
                    <div class="col-sm-6 col-md-5">
                      <div class="form-group">
                        <label>Card Number</label>
                        <input class="form-control" size="20" type="text" placeholder="XXXX-XXXX-XXXX-XXXX">
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col">
                      <div class="form-group">
                        <label>Name on card</label>
                        <input class="form-control" size="28" type="text">
                      </div>
                    </div>
                    <!-- /.col -->
                    <div class="col">
                      <div class="form-group">
                        <label>Expiration date</label>
                        <input class="form-control" size="3" type="text">
                      </div>
                    </div>
                    <!-- /.col -->
                    <div class="col">
                      <div class="form-group">
                        <label>CVV</label>
                        <input class="form-control" size="3" type="text">
                      </div>
                    </div>
                    <!-- /.col -->
                  </div>
                  <!-- /.row -->
                </div>
                <!-- /.box-inner -->
              </div>
              <!-- /.box -->
              <div class="box">
                <div class="box-inner">
                  <div class="box-title">
                    <h2>Billing address</h2>
                  </div>
                  <!-- /.box-title -->
                  <div class="row">
                    <div class="col">
                      <div class="form-group">
                        <label>City</label>
                        <input class="form-control" size="20" type="text">
                      </div>
                    </div>
                    <div class="col">
                      <div class="form-group">
                        <label>Country</label>
                        <select class="form-control"></select>
                      </div>
                      <!-- /.form-group -->
                    </div>
                    <!-- /.col-* -->
                    <div class="col">
                      <div class="form-group">
                        <label>Postal Code</label>
                        <input class="form-control" size="5" type="text">
                      </div>
                      <!-- /.form-group -->
                    </div>
                    <!-- /.col-* -->
                  </div>
                  <!-- /.row -->
                  <div class="row">
                    <div class="col">
                      <div class="form-group">
                        <label>Street and number</label>
                        <input class="form-control" size="28" type="text">
                      </div>
                      <!-- /.form-group -->
                    </div>
                    <!-- /.col-* -->
                  </div>
                  <!-- /.row -->
                  <div class="form-group">
                    <div class="checkbox">
                      <label><input type="checkbox" name="terms" >I accept terms &amp; conditions</label>
                    </div>
                  </div>
                  <!-- /.form-group -->
                </div>
                <!-- /.box-inner -->
              </div>
              <!-- /.box -->
            </form>
            <div class="next-prev">
              <div class="prev">
                <a href="#" class="btn btn-secondary">Return Back</a>
              </div>
              <!-- /.prev -->
              <div class="next">
                <a href="#" class="btn btn-primary">Process</a>
              </div>
              <!-- /.next -->
            </div>
            <!-- /.next-prev -->
          </div>
          <div class="col-md-4 col-lg-3">
            <div class="sidebar">
              <div class="widget">
                <div class="box">
                  <div class="box-inner">
                    <div class="box-title">
                      <h3>Overview</h3>
                    </div>
                    <!-- /.box-title -->
                    <table class="table table-bordered">
                      <tbody>
                        <tr>
                          <th class="title" colspan="2">Pickup Date</th>
                        </tr>
                        <tr>
                          <td>8.12.2013</td>
                          <td>18:00</td>
                        </tr>
                        <tr>
                          <th class="title" colspan="2">Return Date</th>
                        </tr>
                        <tr>
                          <td>12.12.2013</td>
                          <td>18:00</td>
                        </tr>
                        <tr>
                          <th class="title" colspan="2">Car Model</th>
                        </tr>
                        <tr>
                          <td colspan="2">Toyota, LandCruiser</td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                  <!-- /.box-inner -->
                </div>
                <!-- /.box -->
                <form method="post" action="../external.html?link=http://explorer-html.wearecodevision.com/checkout.html?">
                  <div class="checkbox">
                    <label><input type="checkbox">Return car to same location</label>
                  </div>
                  <!-- /.checkbox -->
                  <div class="checkbox">
                    <label><input type="checkbox">Minimum age: 25</label>
                  </div>
                </form>
                <div class="subtotal">
                  <div class="title">Subtotal</div>
                  <div class="value">$3,680</div>
                </div>
                <div class="box">
                  <div class="box-inner">
                    <div class="box-title">
                      <h3>Extra Perks</h3>
                    </div>
                    <!-- /.box-title -->
                    <table class="table table-bordered">
                      <tbody>
                        <tr>
                          <td class="title">GPS</td>
                          <td class="value">$15</td>
                        </tr>
                        <tr>
                          <td class="title">Baby seat - 2x</td>
                          <td class="value">$40</td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                  <!-- /.box-inner -->
                </div>
                <!-- /.box -->
                <div class="total">
                  <div class="title">Total</div>
                  <div class="value">$4,230</div>
                </div>
              </div>
              <!-- /.widget -->
            </div>
            <!-- /.sidebar -->
          </div>
        </div>
        <!-- /.row  -->
      </div>
      <!-- /.container-fluid -->
    </div>
    <!-- /.main-inner -->
  </div>
  <!-- /.main -->
</div>
@push('styles')
<link href="{{asset('assets/libraries/dropify/dist/css/dropify.min.css')}}" rel="stylesheet" type="text/css">
@endpush
@push('scripts')
<script type="text/javascript" src="{{asset('assets/libraries/dropify/dist/js/dropify.min.js')}}"></script>
<script>
	$(function(){
    thisform.init();
	}), thisform = {
    init : function()
    {
      thisform.i_init();
      // thisform.p_init();
      // thisform.masked_inputs();
    },
		// p_init : function()
		// {
		// 	var i = $("#validate");
		// 	i.parsley().on("form:submit", function() {
    //     $(".masked_input").inputmask('remove');
		// 	});
		// },
    // masked_inputs: function() {
    //   $maskedInput = $(".masked_input"),
    //   $maskedInput.length && $maskedInput.inputmask()
    // },
    i_init: function()
    {
      $(".dropify").dropify({
        messages: {
          "default": "Choose Image",
          replace: "Replace",
          remove: "Remove",
          error: "Error"
        }
      });
    },
  }
</script>
@endpush

@endsection
