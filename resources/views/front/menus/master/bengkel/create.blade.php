@extends('front.layouts.default')
@section('title', 'Bengkel')
@section('content')
<div class="main-wrapper">
  <div class="main">
    <div class="main-inner">
      <div class="page-title">
        <div class="container">
          <h1>Bengkel</h1>
        </div>
      </div>
      <div class="container">
        <nav class="breadcrumb">
          <a class="breadcrumb-item" href="{{url('/')}}">Home</a>
          <a class="breadcrumb-item" href="javascript:void(0);">Bengkel</a>
          <span class="breadcrumb-item active">Register</span>
        </nav>
        <div class="box mb80">
          <form method="post" enctype="multipart/form-data" action="{{url()->current()}}" class="mb80">
            {{ csrf_field() }}
            <h3 class="page-title-small">General Information</h3>
            <div class="row mb30">
              <div class="col-sm-12">
                <div class="form-group">
                  <label>Name</label>
                  <input type="text" name="name" class="form-control">
                </div>
              </div>
            </div>
            <div class="row mb30">
              <div class="col-sm-12">
                <div class="form-group">
                  <label>Photo</label>
                  <div class="image">
                    <input type="file" name="url_photo" class="dropify" data-default-file=""/>
                  </div>
                </div>
              </div>
            </div>
            <h3 class="page-title-small">Contact Information</h3>
              <div class="row mb30">
                <div class="col-sm-6">
                  <div id="location-google-map" style="height: 450px;"></div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label>Address</label>
                    <input type="text" name="address" class="form-control location-google-map-search">
                  </div>
                  <div class="form-group">
                    <label>Latitude</label>
                    <input type="text" name="latitude" class="form-control" id="listing_location_latitude">
                  </div>
                  <div class="form-group">
                    <label>Longitude</label>
                    <input type="text" name="longitude" class="form-control" id="listing_location_longitude">
                  </div>
                  <div class="form-group">
                    <label>Phone</label>
                    <input type="text" name="phone" class="form-control">
                  </div>
                  <div class="form-group">
                    <label>Bengkel Status</label>
                    <div class="checkbox-list">
                      <div class="form-check">
                        <label class="form-check-label">
                          <input type="checkbox" name="mobility" class="form-check-input"> Mobility
                        </label>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            <div class="center">
              <button type="submit" class="btn btn-primary">Submit</a>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@push('styles')
<link href="{{asset('assets/libraries/dropify/dist/css/dropify.min.css')}}" rel="stylesheet" type="text/css">
@endpush
@push('scripts')
<script type="text/javascript" src="{{asset('assets/libraries/dropify/dist/js/dropify.min.js')}}"></script>
<script>
	$(function(){
    thisform.init();
	}), thisform = {
    init : function()
    {
      thisform.i_init();
      // thisform.p_init();
      // thisform.masked_inputs();
    },
		// p_init : function()
		// {
		// 	var i = $("#validate");
		// 	i.parsley().on("form:submit", function() {
    //     $(".masked_input").inputmask('remove');
		// 	});
		// },
    // masked_inputs: function() {
    //   $maskedInput = $(".masked_input"),
    //   $maskedInput.length && $maskedInput.inputmask()
    // },
    i_init: function()
    {
      $(".dropify").dropify({
        messages: {
          "default": "Choose Image",
          replace: "Replace",
          remove: "Remove",
          error: "Error"
        }
      });
    },
  }
</script>
@endpush

@endsection
