@extends('front.layouts.default')
@section('title', 'Bengkel')
@section('content')
<div class="main-wrapper">
  <div class="main">
    <div class="main-inner">
      <div class="page-title">
        <div class="container">
          <h1>Bengkel</h1>
        </div>
      </div>
      <div class="container">
        <nav class="breadcrumb">
          <a class="breadcrumb-item" href="{{url('/')}}">Home</a>
          <a class="breadcrumb-item" href="javascript:void(0);">Bengkel</a>
          <span class="breadcrumb-item active">List</span>
        </nav>
        <div class="table-wrapper">
          <table id="tabledata" class="table table-bordered">
            <thead>
              <tr>
                <th class="min-width center">ID</th>
                <th class="min-width center">Name</th>
                <th class="min-width center">Telepon</th>
                <th class="min-width center">Mobility</th>
                <th class="min-width center">Latitude</th>
                <th class="min-width center">Longitude</th>
                <th class="min-width center">Action</th>
                </tr>
            </thead>
            <tbody>
              @foreach($bengkel as $value)
              <tr>
                <td class="min-width center id">{{$value->id}}</td>
                <td>
                  <div class="avatar squared" style="background-image: url({{asset(Storage::disk('public')->url($value->filepath))}})"></div>
                  <h2>
                    <a href="#">{{$value->name}}</a>
                    <span>{{$value->address}}</span>
                  </h2>
                </td>
                <td class="min-width no-wrap center">
                  <span class="tag">{{$value->no_telp}}</span>
                </td>
                <td class="min-width center">
                  <input type="checkbox" class="form-check-input" checked>
                </td>
                <td class="min-width no-wrap center">
                  <span class="tag">{{$value->latitude}}</span>
                </td>
                <td class="min-width no-wrap center">
                  <span class="tag">{{$value->longitude}}</span>
                </td>
                <td class="min-width center">
                  <div class="btn-group" role="group">
                    <button id="btnGroupDrop1" type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" >
                      Actions
                    </button>
                    <div class="dropdown-menu">
                      <a class="dropdown-item" href="#">Workflow Action</a>
                      <a class="dropdown-item" href="#">Change Values</a>
                      <a class="dropdown-item" href="#">Delete Item</a>
                    </div>
                  </div>
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>

@push('styles')
<link href="{{asset('assets/libraries/datatables/css/dataTables.bootstrap4.css')}}" rel="stylesheet" type="text/css">
@endpush
@push('scripts')
<script type="text/javascript" src="{{asset('assets/libraries/datatables/js/jquery.dataTables.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/libraries/datatables/js/dataTables.bootstrap4.js')}}"></script>
<script>
	$(function(){
    thisform.init();
	}), thisform = {
    init : function()
    {
      thisform.d_init();
    },
    d_init: function() {
      var t = $("#tabledata");
      t.DataTable();
    },
  }
</script>
@endpush

@endsection
