@extends('front.layouts.default')
@section('title', 'Item')
@section('content')
<div class="main-wrapper">
  <div class="main">
    <div class="main-inner">
      <div class="page-title">
        <div class="container">
          <h1>Add Item</h1>
        </div>
      </div>
      <div class="container">
        <nav class="breadcrumb">
          <a class="breadcrumb-item" href="{{url('/')}}">Home</a>
          <a class="breadcrumb-item" href="javascript:void(0);">Bengkel</a>
          <span class="breadcrumb-item active">Add Item</span>
        </nav>
        <div class="box mb80">
          <form method="post" enctype="multipart/form-data" action="{{url()->current()}}" class="mb80">
            {{ csrf_field() }}
            <h3 class="page-title-small">Item Information</h3>
            <div class="row mb30">
              <div class="col-sm-12">
                <div class="form-group">
                  <label>Item Name</label>
                  <input type="text" name="nama" class="form-control">
                </div>
              </div>
            </div>
            <div class="row mb30">
              <div class="col-sm-12">
                <div class="form-group">
                  <label>Price</label>
                  <input type="text" name="harga" class="form-control">
                </div>
              </div>
            </div>
            <div class="center">
              <button type="submit" class="btn btn-primary">Submit</a>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@push('styles')
<link href="{{asset('assets/libraries/dropify/dist/css/dropify.min.css')}}" rel="stylesheet" type="text/css">
@endpush
@push('scripts')
<script type="text/javascript" src="{{asset('assets/libraries/dropify/dist/js/dropify.min.js')}}"></script>
<script>
	$(function(){
    thisform.init();
	}), thisform = {
    init : function()
    {
      thisform.i_init();
      // thisform.p_init();
      // thisform.masked_inputs();
    },
		// p_init : function()
		// {
		// 	var i = $("#validate");
		// 	i.parsley().on("form:submit", function() {
    //     $(".masked_input").inputmask('remove');
		// 	});
		// },
    // masked_inputs: function() {
    //   $maskedInput = $(".masked_input"),
    //   $maskedInput.length && $maskedInput.inputmask()
    // },
    i_init: function()
    {
      $(".dropify").dropify({
        messages: {
          "default": "Choose Image",
          replace: "Replace",
          remove: "Remove",
          error: "Error"
        }
      });
    },
  }
</script>
@endpush

@endsection
