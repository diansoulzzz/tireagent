@extends('front.layouts.default')
@section('title', 'Home')
@section('content')
@include('front.includes.maps')
<div class="main-wrapper">
  <div class="main">
    <div class="main-inner">
      <div class="container">
        <div class="content">
          <div class="listing-tabs mt-80">
            <div class="listing-tabs-header">
              <h2>Choose your <strong>agent</strong></h2>
              <ul class="nav nav-tabs" role="tablist">
                <li class="nav-item">
                  <a class="nav-link active" href="#tab-all" data-toggle="tab">All</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="#tab-nearby" data-toggle="tab">Nearby</a>
                </li>
              </ul>
            </div>
            <div class="tab-content">
              <div class="tab-pane active" id="tab-all" role="tabpanel">
                <div class="listing-boxes">
                  <div class="row">
                    @foreach ($allbengkel as $bengkel)
                    <div class="col-sm-3">
                      <div class="listing-box">
                        <div class="listing-box-inner">
                          <a href="{{url('bengkel/bengkel-detail?id='.$bengkel->id)}}" class="listing-box-image">
                            <span class="listing-box-image-content" style="background-image: url({{asset(Storage::disk('public')->url($bengkel->filepath))}})"></span>
                          </a>
                          <div class="listing-box-content">
                            <h2><a href="bengkel-detail.blade.php">{{$bengkel->name}}</a></h2>
                            <h3>{{$bengkel->address}}</h3>
                            <div class="actions">
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    @endforeach
                  </div>
                </div>
              </div>
              <div class="tab-pane" id="tab-nearby" role="tabpanel">
                <div class="listing-boxes">
                  <div class="row">
                    @foreach ($nearbybengkel as $bengkel)
                    <div class="col-sm-3">
                      <div class="listing-box">
                        <div class="listing-box-inner">
                          <a href="{{url('bengkel/bengkel-detail?id='.$bengkel->id)}}" class="listing-box-image">
                            <span class="listing-box-image-content" style="background-image: url({{asset(Storage::disk('public')->url($bengkel->filepath))}})"></span>
                          </a>
                          <div class="listing-box-content">
                            <h2><a href="bengkel-detail.blade.php">{{$bengkel->name}}</a></h2>
                            <h3>{{$bengkel->address}}</h3>
                            <div class="actions">
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    @endforeach
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
