@extends('front.layouts.default')
@section('title', 'Change Password')
@section('content')
<div class="main-wrapper">
  <div class="main">
    <div class="main-inner">
      <div class="page-title">
        <div class="container">
          <h1>Change Password
          </h1>
          <!-- /.page-title-actions -->
        </div>
        <!-- /.container-->
      </div>
      <!-- /.page-title -->
      <div class="container">
        <nav class="breadcrumb">
          <a class="breadcrumb-item" href="{{url('/')}}">Home</a>
          <span class="breadcrumb-item active">Change Password</span>
        </nav>
        <div class="row mb80">
          <div class="col-sm-4 offset-sm-4">
            <h3 class="page-title-small">Change Password</h3>
            <form method="post" enctype="multipart/form-data" action="{{url()->current()}}" class="mb80">
              {{ csrf_field() }}
              <div class="form-group">
                <label for="login-form-password">Old Password</label>
                <input type="password" class="form-control" name="pw1" id="login-form-password">
              </div>
              <div class="form-group">
                <label for="login-form-password">New Password</label>
                <input type="password" class="form-control" name="pw2" id="login-form-password">
              </div>

              <button type="submit" class="btn btn-primary pull-right">Change</button>
            </form>
          </div>
          <!-- /.col-sm-4 -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </div>
    <!-- /.main-inner -->
  </div>
  <!-- /.main -->
</div>
@push('styles')
<link href="{{asset('assets/libraries/dropify/dist/css/dropify.min.css')}}" rel="stylesheet" type="text/css">
@endpush
@push('scripts')
<script type="text/javascript" src="{{asset('assets/libraries/dropify/dist/js/dropify.min.js')}}"></script>
<script>
	$(function(){
    thisform.init();
	}), thisform = {
    init : function()
    {
      thisform.i_init();
      // thisform.p_init();
      // thisform.masked_inputs();
    },
		// p_init : function()
		// {
		// 	var i = $("#validate");
		// 	i.parsley().on("form:submit", function() {
    //     $(".masked_input").inputmask('remove');
		// 	});
		// },
    // masked_inputs: function() {
    //   $maskedInput = $(".masked_input"),
    //   $maskedInput.length && $maskedInput.inputmask()
    // },
    i_init: function()
    {
      $(".dropify").dropify({
        messages: {
          "default": "Choose Image",
          replace: "Replace",
          remove: "Remove",
          error: "Error"
        }
      });
    },
  }
</script>
@endpush

@endsection
