@extends('front.layouts.default')
@section('title', 'User Detail')
@section('content')
<div class="main-wrapper">
  <div class="main">
    <div class="main-inner">
      <div class="page-title">
        <div class="container">
          <h1>User Detail
          </h1>
          <!-- <div class="page-title-actions">
            <div class="switcher">
              <strong>Currency</strong>
              <ul>
                <li class="active"><a href="#">USD</a></li>
                <li><a href="#">EUR</a></li>
              </ul>
            </div>
            <div class="switcher">
              <strong>Language</strong>
              <ul>
                <li class="active"><a href="#">EN</a></li>
                <li><a href="#">FR</a></li>
                <li><a href="#">DE</a></li>
              </ul>
            </div>
          </div> -->
        </div>
        <!-- /.container-->
      </div>
      <!-- /.page-title -->
      <div class="container">
        <nav class="breadcrumb">
          <a class="breadcrumb-item" href="{{url('/')}}">Home</a>
          <span class="breadcrumb-item active">Change Profile</span>
        </nav>
        <form method="post" action="{{url()->current()}}">
          {{ csrf_field() }}
          <input name="id" type="text" class="form-control" value="{{$detail->id}}" hidden>
          <div class="row mb80">
            <div class="col-sm-4 offset-sm-4">
              <h3 class="page-title-small">Change Profile</h3>
              <!-- /.form-group -->

              <div class="form-group">
                <label for="">Name</label>
                <input name="name" type="text" class="form-control" value="{{$detail->name}}">
              </div>
              <!-- /.form-group -->
              <div class="form-group">
                <label for="">E-mail</label>
                <input name="email"  type="email" class="form-control"value="{{$detail->email}}" disabled>
              </div>
              <!-- /.form-group -->
              <div class="center">

                <!-- /.form-group -->
                <div class="form-group-btn">
                  <br>
                  <button type="submit" class="btn btn-primary pull-right">Change</button>
                </div>
                <!-- /.form-group-btn -->
              </div>
              <!-- /.center -->
            </div>
            <!-- /.col-* -->
          </div>
          <!-- /.row -->
        </form>
      </div>
      <!-- /.container-fluid -->
    </div>
    <!-- /.main-inner -->
  </div>
  <!-- /.main -->
</div>
@endsection
