@extends('front.layouts.default')
@section('title', 'Register')
@section('content')
<div class="main-wrapper">
  <div class="main">
    <div class="main-inner">
      <div class="page-title">
        <div class="container">
          <h1>Register
          </h1>
          <!-- <div class="page-title-actions">
            <div class="switcher">
              <strong>Currency</strong>
              <ul>
                <li class="active"><a href="#">USD</a></li>
                <li><a href="#">EUR</a></li>
              </ul>
            </div>
            <div class="switcher">
              <strong>Language</strong>
              <ul>
                <li class="active"><a href="#">EN</a></li>
                <li><a href="#">FR</a></li>
                <li><a href="#">DE</a></li>
              </ul>
            </div>
          </div> -->
        </div>
        <!-- /.container-->
      </div>
      <!-- /.page-title -->
      <div class="container">
        <nav class="breadcrumb">
          <a class="breadcrumb-item" href="{{url('/')}}">Home</a>
          <span class="breadcrumb-item active">Register</span>
        </nav>
        <form method="post" action="{{url()->current()}}">
          {{ csrf_field() }}
          <div class="row mb80">
            <div class="col-sm-4 offset-sm-4">
              <h3 class="page-title-small">Register</h3>
              <!-- /.form-group -->
              <div class="form-group">
                <label for="">Name</label>
                <input name="name" type="text" class="form-control">
              </div>
              <!-- /.form-group -->
              <div class="form-group">
                <label for="">E-mail</label>
                <input name="email"  type="email" class="form-control">
              </div>
              <!-- /.form-group -->
              <div class="form-group">
                <label for="">Password</label>
                <input name="password" type="password" class="form-control">
              </div>
              <!-- /.form-group -->
              <div class="form-group">
                <label for="">Retype Password</label>
                <input name="password_confirmation" type="password" class="form-control">
              </div>
              <!-- /.form-group -->
              <div class="center">
                <div class="checkbox mb30">
                  <label>
                    <input type="checkbox"> By signing up, you agree with the <a href="terms-conditions.html">terms and conditions</a>.
                  </label>
                </div>
                <!-- /.form-group -->
                <div class="form-group-btn">
                  <button type="submit" class="btn btn-primary pull-right">Create Account</button>
                </div>
                <!-- /.form-group-btn -->
              </div>
              <!-- /.center -->
            </div>
            <!-- /.col-* -->
          </div>
          <!-- /.row -->
        </form>
      </div>
      <!-- /.container-fluid -->
    </div>
    <!-- /.main-inner -->
  </div>
  <!-- /.main -->
</div>
@endsection
