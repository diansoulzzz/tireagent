<!DOCTYPE html>
<html lang="en">
<head>
  <title>@yield('title') | Tire Agent</title>
  @include('front.includes.meta')
  @stack('styles')
</head>
<body class="">
<div class="page-wrapper">
  @include('front.includes.header')
  @yield('content')
  @include('front.includes.footer')
</div>
@include('front.includes.modal')
<div class="side-overlay"></div>
@include('front.includes.jsscript')
@stack('scripts')
</body>
</html>
