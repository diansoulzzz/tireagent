<div class="header-wrapper">
  <div class="header">
    <div class="container">
      <div class="header-inner">
        <div class="navigation-toggle toggle">
          <span></span>
          <span></span>
          <span></span>
        </div>
        <!-- /.header-toggle -->
        <div class="header-logo">
          <a href="{{url('/')}}">
            <img src="{{asset('assets/img/logo.svg')}}" class="svg" alt="Home">
          </a>
          <a href="{{url('/')}}" class="header-title">Tire Agent</a>
        </div>

        <!-- /.header-logo -->
        <div class="header-nav">
          <div class="primary-nav-wrapper">
            <ul class="nav">
              <!-- <li class="nav-item">
                <a href="{{url('/')}}" class="nav-link {{Request::is('/') ? 'active' : ''}}">Home</a>
              </li> -->
              <!-- <li class="nav-item">
                <a href="{{url('/order')}}" class="nav-link {{Request::is('/order') ? 'active' : ''}}">Order</a>
              </li> -->
            </ul>
          </div>
          <!-- /.primary-nav-wrapper -->
        </div>
        <div class="header-actions" style="margin: 0 0 0 0;">
          <div class="primary-nav-wrapper horizontal">
            <ul class="nav nav-pills">
              @if(!Session::has('token'))
              <li class="nav-item">
                <a href="{{url('register')}}" class="nav-link">Register</a>
              </li>
              <li class="nav-item">
                <a href="{{url('login')}}" class="nav-link">Login</a>
              </li>
              @else
              <li class="nav-item has-sub-menu">
                <a href="javascript:void(0);" class="nav-link">Bengkel</a>
                <ul class="sub-menu">
                  <li><a href="{{url('bengkel/register')}}">New Bengkel</a></li>
                  <li><a href="{{url('item/add')}}">New Item</a></li>
                  <li><a href="{{url('bengkel/list')}}">Bengkel List</a></li>
                </ul>
              </li>
              <li class="nav-item has-sub-menu">
                <a href="javascript:void(0);" class="nav-link">{{ucfirst(Session::get('token.users.name'))}}</a>
                <ul class="sub-menu">
                  @if(Session::get('token.users.status') == 1)
                    <li><a href="{{url('profile/register')}}">New User</a></li>
                    <li><a href="{{url('profile/list')}}">User List</a></li>
                  @endif
                  <li><a href="{{url('order/list')}}">Order List</a></li>
                  <li><a href="{{url('profile/detail?id='.Session::get('token.users.id'))}}x">Change Profile</a></li>
                  <li><a href="{{url('profile/changepassword')}}">Change Password</a></li>
                  <li><a href="{{url('logout')}}">Logout</a></li>
                </ul>
              </li>
              @endif
            </ul>
          </div>
        </div>
        <!-- /.header-actions -->
      </div>
      <!-- /.header-inner -->
    </div>
    <!-- /.container -->
  </div>
</div>
