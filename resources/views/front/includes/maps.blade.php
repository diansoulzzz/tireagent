<div class="map-wrapper mb80">
  <div class="map" style="height: 740px;">
    <div class="map-inner">
      <div class="map-object">
        <div id="map-object"></div>
        <div class="container">
          <div class="map-toolbar map-toolbar-top-right">
            <div class="map-toolbar-group">
              <div id="map-toolbar-action-zoom-in" class="map-toolbar-group-item"><i class="fa fa-plus"></i></div>
              <!-- /.map-toolbar-group-item -->
              <div id="map-toolbar-action-zoom-out" class="map-toolbar-group-item"><i class="fa fa-minus"></i></div>
              <!-- /.map-toolbar-group-item -->
            </div>
            <!-- /.map-toolbar-group -->
            <div class="map-toolbar-group">
              <div id="map-toolbar-action-current-position" class="map-toolbar-group-item"><i class="fa fa-location-arrow"></i></div>
              <!-- /.map-toolbar-group-item -->
              <div id="map-toolbar-action-fullscreen" class="map-toolbar-group-item"><i class="fa fa-arrows-alt"></i></div>
              <!-- /.map-toolbar-group-item -->
            </div>
            <!-- /.map-toolbar-group -->
            <div class="map-toolbar-group">
              <div id="map-toolbar-action-roadmap" class="map-toolbar-group-item">Roadmap</div>
              <!-- /.map-toolbar-group-item -->
              <div id="map-toolbar-action-satellite" class="map-toolbar-group-item">Satellite</div>
              <!-- /.map-toolbar-group-item -->
              <div id="map-toolbar-action-terrain" class="map-toolbar-group-item">Terrain</div>
              <!-- /.map-toolbar-group-item -->
              <div id="map-toolbar-action-hybrid" class="map-toolbar-group-item">Hybrid</div>
              <!-- /.map-toolbar-group-item -->
            </div>
            <!-- /.map-toolbar-group -->
          </div>
          <!-- /.map-toolbar -->
        </div>
        <!-- /.container -->
        <div class="map-filter-horizontal-wrapper">
          <div class="container">
            <!-- <div class="map-filter-horizontal">
              <form action="{{url('')}}" method="post">
                <div class="row">
                  <div class="col-sm-3 form-group">
                    <input type="text" class="form-control form-control-no-border" placeholder="Search by keyword ...">
                  </div>
                  <div class="col-sm-3 form-group">
                    <select class="form-control form-control-no-border">
                      <option>Location</option>
                      <option>Chicago</option>
                      <option>San Francisco</option>
                      <option>Seattle</option>
                      <option>New York</option>
                      <option>Washington</option>
                    </select>
                  </div>
                  <div class="col-sm-3 form-group">
                    <input type="text" class="form-control form-control-no-border" placeholder="Price to">
                  </div>
                  <div class="col-sm-3">
                    <button type="submit" class="btn btn-block">Filter Listings</button>
                  </div>
                </div>
              </form>
            </div> -->
            <!-- /.map-filter -->
          </div>
          <!-- /.container -->
        </div>
        <!-- /.map-filter-wrapper -->
      </div>
      <!-- /#map-object -->
    </div>
    <!-- /.map-inner -->
  </div>
  <!-- /.map -->
</div>
