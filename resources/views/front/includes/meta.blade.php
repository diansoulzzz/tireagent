<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
<link href="https://fonts.googleapis.com/css?family=Lato:300,400,700" rel="stylesheet">
<link href="{{asset('assets/libraries/slick/slick.css')}}" rel="stylesheet" type="text/css">
<link href="{{asset('assets/libraries/slick/slick-theme.css')}}" rel="stylesheet" type="text/css">
<link href="{{asset('assets/css/trackpad-scroll-emulator.css')}}" rel="stylesheet" type="text/css">
<link href="{{asset('assets/css/chartist.min.css')}}" rel="stylesheet" type="text/css">
<link href="{{asset('assets/css/jquery.raty.css')}}" rel="stylesheet" type="text/css">
<link href="{{asset('assets/fonts/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css">
<link href="{{asset('assets/css/nouislider.min.css')}}" rel="stylesheet" type="text/css">
<link href="{{asset('assets/css/explorer-red.css')}}" rel="stylesheet" type="text/css">
<link rel="shortcut icon" type="image/x-icon" href="{{asset('assets/img/favicon.png')}}">
<title>Directory Application Kit - Explorer</title>

<style>
.nav-pills .nav-item .nav-link {
  background-color: white;
  color: #888;
}
</style>
