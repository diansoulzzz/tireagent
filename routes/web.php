<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['prefix' => '/'], function () {
  Route::get('/', ['uses' =>'Front\Home\FrontHome@Index'])->name('home');
  Route::get('login', ['uses' =>'Front\Account\FrontLogin@Index'])->name('login');
  Route::post('login', ['uses' =>'Front\Account\FrontLogin@Authenticate']);
  Route::get('logout',['uses' =>'Front\Account\FrontLogin@UnAuthenticate']);
  Route::get('register/user', ['uses' => 'Front\Account\FrontRegister@Send']);
  Route::get('register', ['uses' =>'Front\Account\FrontRegister@Index']);
  Route::post('register', ['uses' =>'Front\Account\FrontRegister@Send']);

  Route::get('bengkel/bengkel-detail', ['uses' =>'Front\Transaksi\FrontTransaksi@IndexDetail']);
  Route::get('bengkel/maps', ['uses' =>'Front\Home\FrontHome@GetBengkelMap']);
  Route::get('bengkel/nearby', ['uses' =>'Front\Home\FrontHome@GetBengkelNearby']);

  Route::group(['prefix' => '/','middleware'=> ['web','revalidate','token']], function () {
    // Route::get('profile', ['uses' =>'Front\Account\FrontLogin@Check']);
    Route::group(['prefix' => 'profile'], function () {
      Route::get('/', ['uses' =>'Front\Profile\FrontProfile@Index']);
      Route::get('changepassword', ['uses' =>'Front\Profile\FrontChangePassword@Index']);
      Route::post('changepassword', ['uses' =>'Front\Profile\FrontChangePassword@Send']);
      Route::get('list', ['uses' =>'Front\Profile\FrontProfile@IndexList']);
      Route::get('detail', ['uses' =>'Front\Profile\FrontProfile@IndexDetail']);
      Route::post('detail', ['uses' =>'Front\Profile\FrontProfile@ChangeDetail']);
    });
    Route::group(['prefix' => 'order'], function () {
      Route::get('list', ['uses' =>'Front\Transaksi\FrontTransaksi@IndexOrderList']);
      Route::get('cancel', ['uses' =>'Front\Transaksi\FrontTransaksi@CancelOrder']);
    });

    Route::group(['prefix' => 'item'], function () {
      Route::get('add', ['uses' =>'Front\Master\Item\FrontMasterItem@Index']);
      Route::post('add', ['uses' =>'Front\Master\Item\FrontMasterItem@Send']);
    });

    Route::group(['prefix' => 'bengkel'], function () {
      Route::post('bengkel-detail', ['uses' =>'Front\Transaksi\FrontTransaksi@Send']);
      Route::get('register', ['uses' =>'Front\Master\Bengkel\FrontMasterBengkel@Index']);
      Route::post('register', ['uses' =>'Front\Master\Bengkel\FrontMasterBengkel@Send']);
      Route::get('list', ['uses' =>'Front\Master\Bengkel\FrontMasterBengkel@IndexList']);
    });

    Route::group(['prefix' => '{users}'], function ($users) {
      Route::get('bengkel', ['uses' =>'Front\Master\Bengkel\FrontMasterBengkel@Index']);
      Route::group(['prefix' => '{bengkel}'], function () {

      });
    });
  });
});
