<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('transaksi/detail','Api\Master\Bengkel\ApiMasterBengkel@GetDetail');
Route::get('bengkel/all','Api\Master\Bengkel\ApiMasterBengkel@GetAllBengkel');
Route::get('bengkel/nearby','Api\Master\Bengkel\ApiMasterBengkel@GetNearByBengkel');
Route::get('item/all','Api\Master\Item\ApiMasterBengkel@GetAllBen');
Route::post('user/register', 'Api\Account\ApiAccount@StoreData');

Route::group(['middleware' => 'auth:api'], function () {
  Route::get('transaksi/order/list','Api\Transaksi\ApiTransaksi@GetOrderList');
  Route::post('transaksi/order','Api\Transaksi\ApiTransaksi@Order');
  Route::post('order/cancel','Api\Transaksi\ApiTransaksi@CancelOrder');

  Route::get('user', 'Api\Account\ApiAccount@GetData');
  Route::get('user/info', 'Api\Account\ApiAccount@GetUserInfo');

  Route::get('bengkel/list','Api\Master\Bengkel\ApiMasterBengkel@GetData');
  Route::post('bengkel/register', 'Api\Master\Bengkel\ApiMasterBengkel@StoreData');

  Route::get('item/list','Api\Master\Item\ApiMasterItem@GetData');
  Route::post('item/add', 'Api\Master\Item\ApiMasterItem@StoreData');

  Route::post('profile/changepassword', 'Api\Profile\ApiMasterProfile@ChangePassword');
  Route::get('profile/list','Api\Profile\ApiMasterProfile@GetData');
  Route::get('profile/detail','Api\Profile\ApiMasterProfile@GetDetail');

  Route::post('profile/detail','Api\Profile\ApiMasterProfile@ChangeDetail');
});
