<?php
namespace App\Http\Controllers\Api\Account;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;
use Validator;
use Illuminate\Support\Facades\Auth;
use App\Models\User;

class ApiAccount extends Controller
{
  public function StoreData(Request $request)
  {
    // $status = ['code'=>'0','message'=>'error'];
    // $result = [
    //   'status' => $status,
    //   'request' => $request->all(),
    // ];
    // return $result;
    $validator = Validator::make($request->all(), [
        'email' => 'required',
        'password' => 'required',
    ]);
    
    if ($validator->fails())
    {
      $status = ['code'=>'0','message'=>'error'];
      $result = [
        'status' => $status,
        'request' => $request->all(),
      ];
      return $result;
    }
    $user = new User();
    $user->name = $request->input('name');
    $user->password = bcrypt($request->input('password'));
    $user->email = $request->input('email');
    $user->save();
    $status = ['code'=>'1','message'=>'OK'];
    $result = [
      'status' => $status,
      'request' => $request->all(),
    ];
    return $result;
  }
  public function GetUserInfo(Request $request)
  {
    return response()->json($request->user());
  }
  public function GetData(Request $request)
  {
    $data = User::get();
    if ($data->isEmpty())
    {
      $status = ['code'=>'0','message'=>'error'];
      $result = [
        'status' => $status,
        'request' => $request->all(),
      ];
    }
    else {
      $status = ['code'=>'1','message'=>'OK'];
      $result = [
        'status' => $status,
        'result' => $data,
        'request' => $request->all(),
      ];
    }
    return response()->json($result);
  }
  protected function validator(array $data)
  {
      return Validator::make($data, [
          'email' => 'required',
          'password' => 'required',
      ]);
  }
  public function Logout()
  {
    Auth::logout();
    return redirect(route('login'));
  }
}
