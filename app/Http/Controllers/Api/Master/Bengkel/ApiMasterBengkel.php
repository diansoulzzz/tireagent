<?php
namespace App\Http\Controllers\Api\Master\Bengkel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;
use Validator;
use Illuminate\Support\Facades\Auth;
use App\Models\Bengkel;
use App\Models\Item;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;

class ApiMasterBengkel extends Controller
{
  public function GetAllBengkel(Request $request)
  {
    $data = Bengkel::with(['items','user'])->get();
    if (!isset($data))
    {
      $status = ['code'=>'0','message'=>'error'];
      $result = [
        'status' => $status,
        'request' => $request->all(),
      ];
    }else {
      $status = ['code'=>'1','message'=>'OK'];
      $result = [
        'status' => $status,
        'result' => $data,
        'request' => $request->all(),
      ];
    }
    return response()->json($result);
  }
  public function GetNearByBengkel(Request $request)
  {
    // $geocode = app('geocoder')->geocode('36.82.18.14')->get()->first();
    // $lat = $geocode->getCoordinates()->getLatitude();
    // $lng = $geocode->getCoordinates()->getLongitude();

    $data = DB::select(DB::raw(
      "SELECT id, name, address,no_telp,mobility,filepath, latitude, longitude, (3959 * acos( cos( radians('-7.271837199999999') ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians('112.74499509999998') ) + sin( radians('-7.271837199999999') ) * sin( radians( latitude ) ) ) ) AS distance
      FROM bengkel HAVING distance < '5' ORDER BY distance LIMIT 0 , 20"
    ));

    if (!isset($data))
    {
      $status = ['code'=>'0','message'=>'error'];
      $result = [
        'status' => $status,
        'request' => $request->all(),
      ];
    }else {
      $status = ['code'=>'1','message'=>'OK'];
      $result = [
        'status' => $status,
        'result' => $data,
        'request' => $request->all(),
      ];
    }
    return response()->json($result);
  }
  public function StoreData(Request $request)
  {
    $inputs = json_decode($request->input('inputs'),true);
    $request->offsetUnset('inputs');
    $request->merge($inputs);
    // $status = ['code'=>'1','message'=>'OK'];
    // $result = [
    //   'status' => $status,
    //   'request' => $request->all(),
    // ];
    // return $result;
    // return $request->input('inputs');
    // if ($request->hasFile('url_photo')) {
    //   $status = ['code'=>'1','message'=>'OK'];
    //   $result = [
    //     'status' => $status,
    //     'request' => $request->all(),
    //     'response' => Storage::disk('public')->put('bengkel', $request->file('url_photo')),
    //   ];
    //   return $result;
    // }
    // else {
    //   $status = ['code'=>'0','message'=>'error'];
    //   $result = [
    //     'status' => $status,
    //     'request' => $request->all(),
    //   ];
    //   return $result;
    // }
    $status = ['code'=>'0','message'=>'error'];
    $result = [
      'status' => $status,
      'request' => $request->all(),
    ];
    $validator = Validator::make($request->all(), [
      'name' => 'required',
      'address' => 'required',
      'latitude' => 'required',
      'longitude' => 'required',
      'phone' => 'required',
    ]);
    if ($validator->fails())
    {
      $status = ['code'=>'0','message'=>'error'];
      $result = [
        'status' => $status,
        'request' => $request->all(),
      ];
      return $result;
    }
    $bengkel = new Bengkel();
    $bengkel->name = $request->input('name');
    $bengkel->address = $request->input('address');
    $bengkel->no_telp = $request->input('phone');
    if ($request->has('mobility'))
    {
      $bengkel->mobility = '1';
    }
    else {
      $bengkel->mobility = '0';
    }
    $bengkel->latitude = $request->input('latitude');
    $bengkel->longitude = $request->input('longitude');
    $bengkel->users_id = $request->user()->id;
    if ($request->hasFile('url_photo')) {
      $bengkel->filepath = Storage::disk('public')->put('bengkel', $request->file('url_photo'));
    }
    $bengkel->save();
    $item = new Item();
    $item->nama = 'Tambal Ban';
    $item->harga = '10000';
    $bengkel->items()->save($item);
    // $item->bengkel_id = $bengkel->id;
    // $item->save();
    // id, name, address, no_telp, mobility, filepath, latitude, longitude, users_id

    // $user = new User();
    // $user->name = $request->input('name');
    // $user->password = bcrypt($request->input('password'));
    // $user->email = $request->input('email');
    // $user->save();
    $status = ['code'=>'1','message'=>'OK'];
    $result = [
      'status' => $status,
      'request' => $request->all(),
    ];
    return $result;
  }
  public function GetUserInfo(Request $request)
  {
    return response()->json($request->user());

  }

  public function GetDetail(Request $request)
  {
    // $data = Bengkel::find($request->input('id'))->with('item');
    $data = Bengkel::with(['items','user'])->where('id',$request->input('id'))->first();
    if (!isset($data))
    {
      $status = ['code'=>'0','message'=>'error'];
      $result = [
        'status' => $status,
        'request' => $request->all(),
      ];
    }
    else {
      $status = ['code'=>'1','message'=>'OK'];
      $result = [
        'status' => $status,
        'result' => $data,
        'request' => $request->all(),
      ];
    }
    return response()->json($result);
  }

  public function GetData(Request $request)
  {
    $data = Bengkel::get();
    if ($data->isEmpty())
    {
      $status = ['code'=>'0','message'=>'error'];
      $result = [
        'status' => $status,
        'request' => $request->all(),
      ];
    }
    else {
      $status = ['code'=>'1','message'=>'OK'];
      $result = [
        'status' => $status,
        'result' => $data,
        'request' => $request->all(),
      ];
    }
    return response()->json($result);
  }
  public function Logout()
  {
    Auth::logout();
    return redirect(route('login'));
  }
}
