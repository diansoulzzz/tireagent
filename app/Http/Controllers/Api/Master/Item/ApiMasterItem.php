<?php
namespace App\Http\Controllers\Api\Master\Item;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;
use Validator;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\Item;
use App\Models\Bengkel;

class ApiMasterItem extends Controller
{

  public function StoreData(Request $request)
  {
    return $request;
    $status = ['code'=>'0','message'=>'error'];
    $result = [
      'status' => $status,
      'request' => $request->all(),
    ];

    // $validator = Validator::make($request->all(), [
    //   'nama' => 'required',
    //   'harga' => 'required',
    // ]);
    //
    // if ($validator->fails())
    // {
    //   $status = ['code'=>'0','message'=>'error'];
    //   $result = [
    //     'status' => $status,
    //     'request' => $request->all(),
    //   ];
    //   //return response()->json($result);
    //   return $result;
    // }

    // $data = Bengkel::with(['items','user'])->where('id',$request->input('id'))->first();
    // if (!isset($data))
    // {
    //   $status = ['code'=>'0','message'=>'error'];
    //   $result = [
    //     'status' => $status,
    //     'request' => $request->all(),
    //   ];
    // }
    // else {
    //   $status = ['code'=>'1','message'=>'OK'];
    //   $result = [
    //     'status' => $status,
    //     'result' => $data,
    //     'request' => $request->all(),
    //   ];
    // }

    $data = new Item();
    $data->nama = $request->input('nama');
    $data->harga = $request->input('harga');
    $data->bengkel_id = '100';
    $data->save();

    $status = ['code'=>'1','message'=>'OK'];
    $result = [
      'status' => $status,
      'request' => $request->all(),
    ];
    return response()->json($result);
    return $result;
  }

  public function GetUserInfo(Request $request)
  {
    return response()->json($request->user());
  }

  public function GetAllItem(Request $request)
  {
    $data = Item::get();
    if (!isset($data))
    {
      $status = ['code'=>'0','message'=>'error'];
      $result = [
        'status' => $status,
        'request' => $request->all(),
      ];
    }
    else {
      $status = ['code'=>'1','message'=>'OK'];
      $result = [
        'status' => $status,
        'result' => $data,
        'request' => $request->all(),
      ];
    }
    return response()->json($result);
  }

  public function GetData(Request $request)
  {
    $data = Item::get();
    if ($data->isEmpty())
    {
      $status = ['code'=>'0','message'=>'error'];
      $result = [
        'status' => $status,
        'request' => $request->all(),
      ];
    }
    else {
      $status = ['code'=>'1','message'=>'OK'];
      $result = [
        'status' => $status,
        'result' => $data,
        'request' => $request->all(),
      ];
    }
    return response()->json($result);
  }

  protected function validator(array $data)
  {
      return Validator::make($data, [
          'email' => 'required',
          'password' => 'required',
      ]);
  }
  public function Logout()
  {
    Auth::logout();
    return redirect(route('login'));
  }
}
