<?php
namespace App\Http\Controllers\Api\Profile;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;
use Validator;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Crypt;

class ApiMasterProfile extends Controller
{

  public function GetData(Request $request)
  {
    $data = User::get();
    if ($data->isEmpty())
    {
      $status = ['code'=>'0','message'=>'error'];
      $result = [
        'status' => $status,
        'request' => $request->all(),
      ];
    }
    else {
      $status = ['code'=>'1','message'=>'OK'];
      $result = [
        'status' => $status,
        'result' => $data,
        'request' => $request->all(),
      ];
    }
    return response()->json($result);
  }

  public function GetDetail(Request $request)
  {
    $data = User::find($request->input('id'));
    if (!isset($data))
    {
      $status = ['code'=>'0','message'=>'error'];
      $result = [
        'status' => $status,
        'request' => $request->all(),
      ];
    }
    else {
      $status = ['code'=>'1','message'=>'OK'];
      $result = [
        'status' => $status,
        'result' => $data,
        'request' => $request->all(),
      ];
    }
    return response()->json($result);
  }

  public function ChangePassword(Request $request){

    // $validator =  Validator::make($request->all, [
    //     'pw1' => 'required',
    //     'pw2' => 'required',
    // ]);
    //
    // if ($validator->fails())
    // {
    //   $status = ['code'=>'0','message'=>'error'];
    //   $result = [
    //     'status' => $status,
    //     'request' => $request->all(),
    //   ];
    //   return $result;
    // }

    $old_password = $request->input('pw1');
    $password = $request->input('pw2');
    $user=User::find($request->user()->id);
    // return $user;
    if(!Hash::check($old_password,$user->password))
    {
      $status = ['code'=>'0','message'=>'error'];
      $result = [
        'status' => $status,
        'request' => $request->all(),
      ];
      return $result;
    }

    $user->password=bcrypt($password);
    $user->save();

    // $user = new User();
    // $user->name = $request->input('name');
    // $user->password = bcrypt($request->input('pw2'));
    //
    // $user->email = $request->input('email');
    // $user->save();
    $status = ['code'=>'1','message'=>'OK'];
    $result = [
      'status' => $status,
      'request' => $request->all(),
    ];
    return $result;

  }

  public function ChangeDetail(Request $request){

    // $validator =  Validator::make($request->all, [
    //     'pw1' => 'required',
    //     'pw2' => 'required',
    // ]);
    //
    // if ($validator->fails())
    // {
    //   $status = ['code'=>'0','message'=>'error'];
    //   $result = [
    //     'status' => $status,
    //     'request' => $request->all(),
    //   ];
    //   return $result;
    // }

    $name = $request->input('name');
    $user=User::find($request->user()->id);
    // return $user;
      // $status = ['code'=>'0','message'=>'error'];
      // $result = [
      //   'status' => $status,
      //   'request' => $request->all(),
      // ];
      // return $result;

    $user->name=$name;
    $user->save();

    // $user = new User();
    // $user->name = $request->input('name');
    // $user->password = bcrypt($request->input('pw2'));
    //
    // $user->email = $request->input('email');
    // $user->save();
    $status = ['code'=>'1','message'=>'OK'];
    $result = [
      'status' => $status,
      'request' => $request->all(),
    ];
    return $result;

  }

  public function Logout()
  {
    Auth::logout();
    return redirect(route('login'));
  }
}
