<?php
namespace App\Http\Controllers\Api\Transaksi;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;
use Validator;
use Illuminate\Support\Facades\Auth;
use App\Models\HOrder;
use App\Models\Item;
use App\Models\DOrder;
use Carbon\Carbon;

class ApiTransaksi extends Controller
{
  public function GenerateNota($id,$tipe)
  {
    $date = Carbon::parse(Carbon::now()->toDateString())->format("Y/m/d");
    return $tipe.'/'.$date.'/'.sprintf('%04d', $id);
  }
  public function CancelOrder(Request $request)
  {
    $order = HOrder::find($request->input('id'));
    $order->delete();
    $status = ['code'=>'1','message'=>'OK'];
    $result = [
      'status' => $status,
      'request' => $request->all(),
    ];
    return $result;
  }
  public function Order(Request $request)
  {
    // return Auth::user();
    // return $request->all();
    // return response()->json($request->input('name'));
    // return $request->input('url_photo');
    $last_trans = HOrder::where('date','=', Carbon::now()->toDateString())->count()+1;
    $no_invoice = $this->GenerateNota($last_trans,'INV');

    $status = ['code'=>'0','message'=>'error'];
    $result = [
      'status' => $status,
      'request' => $request->all(),
    ];
    $validator = Validator::make($request->all(), [
      'item' => 'required',
    ]);
    if ($validator->fails())
    {
      $status = ['code'=>'0','message'=>'error'];
      $result = [
        'status' => $status,
        'request' => $request->all(),
      ];
      return $result;
    }
    $items = [];
    $grandtotal=0;
    foreach ($request->input('item') as $key => $itemid) {
      $item = Item::find($itemid);
      $items[] = $item;
      $grandtotal=$grandtotal+$item['harga'];
      $bengkel_id=$item['bengkel_id'];
    }
    // return $items;
    // id, date, grandtotal, users_id, bengkel_id
    $order = new HOrder();
    $order->kodenota = $no_invoice;
    $order->date =Carbon::now()->toDateString();
    $order->grandtotal = $grandtotal;
    $order->bengkel_id = $bengkel_id;
    $order->users_id = Auth::user()->id;
    $order->save();
    foreach ($items as $key => $detail) {
      $d_order = new DOrder();
      $d_order->item_id = $detail['id'];
      $d_order->harga = $detail['harga'];
      $order->d_orders()->save($d_order);
    }
    $status = ['code'=>'1','message'=>'OK'];
    $result = [
      'status' => $status,
      'request' => $request->all(),
    ];
    return $result;
  }
  public function GetUserInfo(Request $request)
  {
    return response()->json($request->user());

  }

  public function GetDetail(Request $request)
  {
    // $data = Bengkel::find($request->input('id'))->with('item');
    $data = Bengkel::with(['items','user'])->where('id',$request->input('id'))->first();
    if (!isset($data))
    {
      $status = ['code'=>'0','message'=>'error'];
      $result = [
        'status' => $status,
        'request' => $request->all(),
      ];
    }
    else {
      $status = ['code'=>'1','message'=>'OK'];
      $result = [
        'status' => $status,
        'result' => $data,
        'request' => $request->all(),
      ];
    }
    return response()->json($result);
  }

  public function GetData(Request $request)
  {
    $data = Bengkel::get();
    if ($data->isEmpty())
    {
      $status = ['code'=>'0','message'=>'error'];
      $result = [
        'status' => $status,
        'request' => $request->all(),
      ];
    }
    else {
      $status = ['code'=>'1','message'=>'OK'];
      $result = [
        'status' => $status,
        'result' => $data,
        'request' => $request->all(),
      ];
    }
    return response()->json($result);
  }

  public function GetOrderList(Request $request)
  {
    $data = HOrder::get();
    if ($data->isEmpty())
    {
      $status = ['code'=>'0','message'=>'error'];
      $result = [
        'status' => $status,
        'request' => $request->all(),
      ];
    }
    else {
      $status = ['code'=>'1','message'=>'OK'];
      $result = [
        'status' => $status,
        'result' => $data,
        'request' => $request->all(),
      ];
    }
    return response()->json($result);
  }

  public function Logout()
  {
    Auth::logout();
    return redirect(route('login'));
  }
}
