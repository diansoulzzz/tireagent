<?php
namespace App\Http\Controllers\Front\Home;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;
use Validator;
use Illuminate\Support\Facades\Auth;
use \GuzzleHttp\Exception\GuzzleException;
use \GuzzleHttp\Client;
use Illuminate\Support\Facades\Storage;

class FrontHome extends Controller
{
  public function Index(Request $request)
  {
    $allbengkel = $this->getBengkel($request);
    $nearbybengkel = $this->GetBengkelNearby($request);
    // return $nearbybengkel;
    return view('front.menus.home.content',compact('allbengkel','nearbybengkel'));
  }

  public function GetBengkelMap(Request $request)
  {
    $client = new Client();
    $response = $client->get(url('api/bengkel/all'), [
      'headers' => [
          // 'Authorization' => 'Bearer '.session()->get('token.access_token'),
      ],
    ]);
    $result = json_decode((string) $response->getBody());
    $maps = [];
    if ($result->status->code)
    {
      foreach ($result->result as $key => $bengkel) {
        $info = [
          'id'=>$bengkel->id,
          'url'=>url('bengkel/bengkel-detail?id='.$bengkel->id),
          'title'=>$bengkel->name,
          'address'=>$bengkel->address,
          'price' => false,
          'thumbnail'=>asset(Storage::disk('public')->url($bengkel->filepath)),
          'verified'=>$bengkel->mobility,
          'phone'=>$bengkel->no_telp,
          'latitude'=>$bengkel->latitude,
          'longitude'=>$bengkel->longitude,
          'owner'=>$bengkel->user,
          'items'=>$bengkel->items,
        ];
        $maps[] = $info;
      }
    }
    return response()->json($maps);
  }

  public function getBengkel(Request $request)
  {
    $client = new Client();
    $response = $client->get(url('api/bengkel/all'), [
      'headers' => [
          // 'Authorization' => 'Bearer '.session()->get('token.access_token'),
      ],
      // 'form_params' => [
      //     'id' => '1'
      // ]
    ]);
    $result = json_decode((string) $response->getBody());
    //return $result;
    $detail = [];
    if ($result->status->code)
    {
      $detail = $result->result;
    }
    return $detail;
    // return response()->json($result);
  }

  public function GetBengkelNearby(Request $request)
  {
    $client = new Client();
    $response = $client->get(url('api/bengkel/nearby'), [
      'headers' => [
          // 'Authorization' => 'Bearer '.session()->get('token.access_token'),
      ],
      // 'form_params' => [
      //     'latitude' => $lat,
      //     'longitude'=> $lng,
      // ]
    ]);
    $result = json_decode((string) $response->getBody());
    // return response()->json($result);
    $detail = [];
    if ($result->status->code)
    {
      $detail = $result->result;
    }
    return $detail;
    // return response()->json($result);
  }
  public function getItem(Request $request)
  {
//    return view('front.menus.profile.detail');

    $client = new Client();
    $response = $client->get(url('api/item/all'), [
      'headers' => [
          'Authorization' => 'Bearer '.session()->get('token.access_token'),
      ],
      // 'form_params' => [
      //     'id' => '1'
      // ]
    ]);
    $result = json_decode((string) $response->getBody());
    $detail = [];
    if ($result->status->code)
    {
      $detail = $result->result;
    }
    return $detail;
  }
}
