<?php
namespace App\Http\Controllers\Front\Transaksi;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;
use Validator;
use Illuminate\Support\Facades\Auth;
use \GuzzleHttp\Exception\GuzzleException;
use \GuzzleHttp\Client;

class FrontTransaksi extends Controller
{
  public function IndexOrderList(Request $request)
  {
    $client = new Client();
    $response = $client->get(url('api/transaksi/order/list'), [
      'headers' => [
          'Authorization' => 'Bearer '.session()->get('token.access_token'),
      ],
    ]);
    $result = json_decode((string) $response->getBody());
    $orderlist = [];
    if ($result->status->code)
    {
      $orderlist = $result->result;
    }
    return view('front.menus.transaksi.order-list',compact('orderlist'));
  }
  public function IndexDetail(Request $request)
  {
    $client = new Client();
    $response = $client->get(url('api/transaksi/detail?id='.$request->input('id')), [
      'headers' => [
          'Authorization' => 'Bearer '.session()->get('token.access_token'),
      ],
      // 'form_params' => [
      //     'id' => '1'
      // ]
    ]);
    $result = json_decode((string) $response->getBody());
    $detail = [];
    if ($result->status->code)
    {
      $detail = $result->result;
    }
    return view('front.menus.transaksi.bengkel-detail',compact('detail'));

  }
  public function Receive(Request $request)
  {
    $client = new Client();
    $response = $client->get(url('api/bengkel/list'), [
      'headers' => [
          'Authorization' => 'Bearer '.session()->get('token.access_token'),
      ],
      // 'form_params' => [
      //     'name' => $name,
      //     'email' => $email,
      //     'password' => $password,
      // ]
    ]);
    $result = json_decode((string) $response->getBody());
    return response()->json($result);
  }
  public function Send(Request $request)
  {
    $senddata =  $request->all();
    $client = new Client();
    $response = $client->post(url('api/transaksi/order'), [
      'headers' => [
          'Authorization' => 'Bearer '.session()->get('token.access_token'),
          'Content-type' => 'application/x-www-form-urlencoded',
      ],
      'form_params' => $senddata
    ]);
    $result = json_decode((string) $response->getBody());
    // return response()->json($result);
    if ($result->status->code)
    {
      return redirect('/');
    }
  }

  public function CancelOrder(Request $request)
  {
    $senddata =  $request->all();
    $client = new Client();
    $response = $client->post(url('api/order/cancel'), [
      'headers' => [
          'Authorization' => 'Bearer '.session()->get('token.access_token'),
          'Content-type' => 'application/x-www-form-urlencoded',
      ],
      'form_params' => $senddata
    ]);
    $result = json_decode((string) $response->getBody());
    // return response()->json($result);
    if ($result->status->code)
    {
      return redirect('/');
    }
  }
}
