<?php
namespace App\Http\Controllers\Front\Profile;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;
use Validator;
use Illuminate\Support\Facades\Auth;
use \GuzzleHttp\Exception\GuzzleException;
use \GuzzleHttp\Client;

class FrontProfile extends Controller
{
  public function Index(Request $request)
  {
    return view('front.menus.profile.list');
  }

  public function IndexDetail(Request $request)
  {
    $client = new Client();
    $response = $client->get(url('api/profile/detail?id='.$request->input('id')), [
      'headers' => [
          'Authorization' => 'Bearer '.session()->get('token.access_token'),
      ],
      // 'form_params' => [
      //     'id' => '1'
      // ]
    ]);
    $result = json_decode((string) $response->getBody());
    $detail = [];
    if ($result->status->code)
    {
      $detail = $result->result;
    }
    return view('front.menus.profile.detail',compact('detail'));

  }


  public function ChangeDetail(Request $request)
  {
    $senddata =  $request->only('name');

    $client = new Client();
    $response = $client->post(url('api/profile/detail'), [
      'headers' => [
          'Authorization' => 'Bearer '.session()->get('token.access_token'),
          'Content-type' => 'application/x-www-form-urlencoded',
      ],
      'form_params' => $senddata
    ]);
    $result = json_decode((string) $response->getBody());
    if ($result->status->code)
    {
      $response = $client->get(url('api/user/info'), [
        'headers' => [
            'Authorization' => 'Bearer '.$request->session()->get('token.access_token'),
        ],
      ]);
      $request->session()->put('token.users', json_decode((string) $response->getBody(), true));
      return redirect('/');
    }
    else{
      return redirect(url()->current());
    }
  }

  public function IndexList(Request $request)
  {
    $client = new Client();
    $response = $client->get(url('api/profile/list'), [
      'headers' => [
          'Authorization' => 'Bearer '.session()->get('token.access_token'),
      ],
      // 'form_params' => [
      //     'name' => $name,
      //     'email' => $email,
      //     'password' => $password,
      // ]
    ]);
    $result = json_decode((string) $response->getBody());
    $profile = [];
    if ($result->status->code)
    {
      $profile = $result->result;
    }
    return view('front.menus.profile.list',compact('profile'));
  }
  public function Receive(Request $request)
  {
    $client = new Client();
    $response = $client->get(url('api/profile/list'), [
      'headers' => [
          'Authorization' => 'Bearer '.session()->get('token.access_token'),
      ],
      // 'form_params' => [
      //     'name' => $name,
      //     'email' => $email,
      //     'password' => $password,
      // ]
    ]);
    $result = json_decode((string) $response->getBody());
    return response()->json($result);
  }
  public function Send(Request $request)
  {
    $senddata =  $request->only('name','email','password');
    $client = new Client();
    $response = $client->post(url('api/profile/register'), [
      'headers' => [
          'Authorization' => 'Bearer '.session()->get('token.access_token'),
          'Content-type' => 'application/x-www-form-urlencoded',
      ],
      'form_params' => $senddata
    ]);
    $result = json_decode((string) $response->getBody());
    if ($result->status->code)
    {
      return redirect('profile/register');
    }
  }
}
