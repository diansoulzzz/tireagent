<?php
namespace App\Http\Controllers\Front\Profile;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;
use Validator;
use Illuminate\Support\Facades\Auth;
use \GuzzleHttp\Exception\GuzzleException;
use \GuzzleHttp\Client;

class FrontChangePassword extends Controller
{
  public function Index(Request $request)
  {
    return view('front.menus.profile.password');
  }

  public function Send(Request $request)
  {
    $senddata =  $request->only('pw1','pw2');

    $client = new Client();
    $response = $client->post(url('api/profile/changepassword'), [
      'headers' => [
          'Authorization' => 'Bearer '.session()->get('token.access_token'),
          'Content-type' => 'application/x-www-form-urlencoded',
      ],
      'form_params' => $senddata
    ]);
    $result = json_decode((string) $response->getBody());
    if ($result->status->code)
    {
      return redirect('/');
    }
    else{
      return redirect(url()->current());
    }
  }
}
