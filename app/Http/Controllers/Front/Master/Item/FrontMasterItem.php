<?php
namespace App\Http\Controllers\Front\Master\Item;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;
use Validator;
use Illuminate\Support\Facades\Auth;
use \GuzzleHttp\Exception\GuzzleException;
use \GuzzleHttp\Client;

class FrontMasterItem extends Controller
{
  public function Index(Request $request)
  {
    return view('front.menus.master.item.create');
  }

  public function Send(Request $request)
  {
    $senddata =  $request->all();
    //return response()->json($result);
    $client = new Client();
    $response = $client->post(url('api/item/add'), [
      'headers' => [
          'Authorization' => 'Bearer '.session()->get('token.access_token'),
          'Content-type' => 'application/x-www-form-urlencoded',
      ],
      'form_params' => $senddata
    ]);
    $result = json_decode((string) $response->getBody());
    if ($result->status->code)
    {
      return redirect('item/add');
    }
    //return response()->json($result);
  }

  public function Receive(Request $request)
  {
    $client = new Client();
    $response = $client->get(url('api/item/list'), [
      'headers' => [
          'Authorization' => 'Bearer '.session()->get('token.access_token'),
      ],
    ]);
    $result = json_decode((string) $response->getBody());
    return response()->json($result);
  }
}
