<?php
namespace App\Http\Controllers\Front\Master\Bengkel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;
use Validator;
use Illuminate\Support\Facades\Auth;
use \GuzzleHttp\Exception\GuzzleException;
use \GuzzleHttp\Client;
use Illuminate\Support\Facades\Storage;

class FrontMasterBengkel extends Controller
{
  public function Index(Request $request)
  {
    // return $this->GetBengkelMap($request);
    return view('front.menus.master.bengkel.create');
  }
  public function IndexList(Request $request)
  {
    $client = new Client();
    $response = $client->get(url('api/bengkel/list'), [
      'headers' => [
          'Authorization' => 'Bearer '.session()->get('token.access_token'),
      ],
    ]);
    $result = json_decode((string) $response->getBody());
    $bengkel = [];
    if ($result->status->code)
    {
      $bengkel = $result->result;
    }
    return view('front.menus.master.bengkel.list',compact('bengkel'));
  }
  // public function Receive(Request $request)
  // {
  //   $client = new Client();
  //   $response = $client->get(url('api/bengkel/list'), [
  //     'headers' => [
  //         'Authorization' => 'Bearer '.session()->get('token.access_token'),
  //     ],
  //   ]);
  //   $result = json_decode((string) $response->getBody());
  //   return response()->json($result);
  // }
  public function Send(Request $request)
  {
    $data =  $request->only('name','address','latitude','longitude','phone','mobility');
    $client = new Client();
    $response = $client->post(url('api/bengkel/register'), [
      'headers' => [
          'Authorization' => 'Bearer '.session()->get('token.access_token'),
      ],
      'multipart' => [
        [
            'name'     => "url_photo",
            'contents' => fopen($request->file('url_photo')->getRealPath(), 'r'),
            'filename' => $request->file('url_photo')->getClientOriginalName()
        ],
        [
            'name'     => 'inputs',
            'contents' => json_encode($data)
        ],
      ]
    ]);
    $result = json_decode((string) $response->getBody());
    if ($result->status->code)
    {
      return redirect('bengkel/register');
    }
  }
}
