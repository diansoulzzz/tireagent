<?php
namespace App\Http\Controllers\Front\Account;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;
use Validator;
use Illuminate\Support\Facades\Auth;
use \GuzzleHttp\Exception\GuzzleException;
use \GuzzleHttp\Client;

class FrontRegister extends Controller
{
  public function Index(Request $request)
  {
    return view('front.menus.account.register');
  }
  public function Send(Request $request)
  {
    //url('oauth/token'); //http://todos.dev/oauth/token
    $validator = $this->validator($request->all());
    if ($validator->fails())
    {
      return redirect()->back()->withErrors($validator)->withInput();
    }
    $name = $request->input('name');
    $email = $request->input('email');
    $password = $request->input('password');
    $client = new Client();
    // $response = $client->post(url('oauth/token'), [
    //     'form_params' => [
    //         'client_id' => env('PASSWORD_CLIENT_ID'),
    //         'client_secret' => env('PASSWORD_CLIENT_SECRET'),
    //         'grant_type' => 'password',
    //         'username' => env('PASSWORD_CLIENT_USER_EMAIL'),
    //         'password' => env('PASSWORD_CLIENT_USER_SECRET'),
    //         'scope' => '*',
    //     ],
    // ]);
    // $auth = json_decode((string) $response->getBody());
    $response = $client->post(url('api/user/register'), [
      'headers' => [
          // 'Authorization' => 'Bearer '.$auth->access_token,
      ],
      'form_params' => [
          'name' => $name,
          'email' => $email,
          'password' => $password,
      ]
    ]);
    $result = json_decode((string) $response->getBody());
    // return response()->json($result);
    if ($result->status->code)
    {
      return redirect('login');
    }
    // return response()->json($result);
  }
  protected function validator(array $data)
  {
      return Validator::make($data, [
          'name' => 'required',
          'email' => 'required',
          'password' => 'required',
      ]);
  }
}
