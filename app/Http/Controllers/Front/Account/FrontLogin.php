<?php
namespace App\Http\Controllers\Front\Account;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;
use Validator;
use Illuminate\Support\Facades\Auth;
use \GuzzleHttp\Exception\GuzzleException;
use \GuzzleHttp\Client;

class FrontLogin extends Controller
{
  public function Index(Request $request)
  {
    return view('front.menus.account.login');
  }
  public function UnAuthenticate(Request $request)
  {
    $request->session()->forget('token');
    return redirect('login');
  }
  public function Check(Request $request)
  {
    $client = new Client();
    $response = $client->get(url('api/user'), [
      'headers' => [
          'Authorization' => 'Bearer '.$request->session()->get('token.access_token'),
      ],
      // 'form_params' => [
      //     'email' => $email,
      //     'password' => $password,
      // ]
    ]);
    $result = json_decode((string) $response->getBody());
    return response()->json($result);

  }
  public function Authenticate(Request $request)
  {
    $validator = $this->validator($request->all());
    if ($validator->fails())
    {
      return redirect()->back()->withErrors($validator)->withInput();
    }
    $email = $request->input('email');
    $password = $request->input('password');
    $client = new Client();
    $response = $client->post(url('oauth/token'), [
        'form_params' => [
            'client_id' => env('PASSWORD_CLIENT_ID'),
            'client_secret' => env('PASSWORD_CLIENT_SECRET'),
            'grant_type' => 'password',
            'username' => $email,
            'password' => $password,
            'scope' => '*',
        ],
    ]);
    $request->session()->put('token', json_decode((string) $response->getBody(), true));
    $response = $client->get(url('api/user/info'), [
      'headers' => [
          'Authorization' => 'Bearer '.$request->session()->get('token.access_token'),
      ],
    ]);
    $request->session()->put('token.users', json_decode((string) $response->getBody(), true));
    // return $request->session()->get('token');
    if ($request->session()->has('token')) {
      return redirect('/');
    }
  }
  protected function validator(array $data)
  {
      return Validator::make($data, [
          'email' => 'required',
          'password' => 'required',
      ]);
  }
}
