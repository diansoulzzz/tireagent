<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 20 Nov 2017 09:50:20 +0000.
 */

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * Class User
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string $password
 * @property string $filepath
 * @property string $remember_token
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @property int $status
 *
 * @property \Illuminate\Database\Eloquent\Collection $bengkels
 * @property \Illuminate\Database\Eloquent\Collection $h_orders
 *
 * @package App\Models
 */
class User extends Authenticatable
{
	use HasApiTokens, Notifiable;
	// use \Illuminate\Database\Eloquent\SoftDeletes;
	public $incrementing = false;

	protected $casts = [
		'id' => 'int',
		'status' => 'int'
	];

	protected $hidden = [
		'remember_token'
	];

	protected $fillable = [
		'name',
		'email',
		'password',
		'filepath',
		'remember_token',
		'status'
	];

	public function bengkels()
	{
		return $this->hasMany(\App\Models\Bengkel::class, 'users_id');
	}

	public function h_orders()
	{
		return $this->hasMany(\App\Models\HOrder::class, 'users_id');
	}
}
