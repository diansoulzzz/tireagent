<?php

/**
 * Created by Reliese Model.
 * Date: Sat, 16 Dec 2017 15:32:57 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Bengkel
 * 
 * @property int $id
 * @property string $name
 * @property string $address
 * @property string $no_telp
 * @property int $mobility
 * @property string $filepath
 * @property string $latitude
 * @property string $longitude
 * @property int $users_id
 * 
 * @property \App\Models\User $user
 * @property \Illuminate\Database\Eloquent\Collection $h_orders
 * @property \Illuminate\Database\Eloquent\Collection $items
 *
 * @package App\Models
 */
class Bengkel extends Eloquent
{
	protected $table = 'bengkel';
	public $timestamps = false;

	protected $casts = [
		'mobility' => 'int',
		'users_id' => 'int'
	];

	protected $fillable = [
		'name',
		'address',
		'no_telp',
		'mobility',
		'filepath',
		'latitude',
		'longitude',
		'users_id'
	];

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class, 'users_id');
	}

	public function h_orders()
	{
		return $this->hasMany(\App\Models\HOrder::class);
	}

	public function items()
	{
		return $this->hasMany(\App\Models\Item::class);
	}
}
