<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 27 Nov 2017 08:47:34 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Item
 * 
 * @property int $id
 * @property string $nama
 * @property float $harga
 * @property int $bengkel_id
 * 
 * @property \App\Models\Bengkel $bengkel
 * @property \Illuminate\Database\Eloquent\Collection $d_orders
 *
 * @package App\Models
 */
class Item extends Eloquent
{
	protected $table = 'item';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'id' => 'int',
		'harga' => 'float',
		'bengkel_id' => 'int'
	];

	protected $fillable = [
		'nama',
		'harga',
		'bengkel_id'
	];

	public function bengkel()
	{
		return $this->belongsTo(\App\Models\Bengkel::class);
	}

	public function d_orders()
	{
		return $this->hasMany(\App\Models\DOrder::class);
	}
}
