<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 18 Dec 2017 17:04:02 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class DOrder
 * 
 * @property int $id
 * @property int $item_id
 * @property int $h_order_id
 * @property string $harga
 * 
 * @property \App\Models\HOrder $h_order
 * @property \App\Models\Item $item
 *
 * @package App\Models
 */
class DOrder extends Eloquent
{
	protected $table = 'd_order';
	public $timestamps = false;

	protected $casts = [
		'item_id' => 'int',
		'h_order_id' => 'int'
	];

	protected $fillable = [
		'item_id',
		'h_order_id',
		'harga'
	];

	public function h_order()
	{
		return $this->belongsTo(\App\Models\HOrder::class);
	}

	public function item()
	{
		return $this->belongsTo(\App\Models\Item::class);
	}
}
