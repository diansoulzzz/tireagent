<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 18 Dec 2017 18:33:26 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class HOrder
 * 
 * @property int $id
 * @property string $kodenota
 * @property \Carbon\Carbon $date
 * @property int $users_id
 * @property float $grandtotal
 * @property int $bengkel_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * 
 * @property \App\Models\Bengkel $bengkel
 * @property \App\Models\User $user
 * @property \Illuminate\Database\Eloquent\Collection $d_orders
 *
 * @package App\Models
 */
class HOrder extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;
	protected $table = 'h_order';

	protected $casts = [
		'users_id' => 'int',
		'grandtotal' => 'float',
		'bengkel_id' => 'int'
	];

	protected $dates = [
		'date'
	];

	protected $fillable = [
		'kodenota',
		'date',
		'users_id',
		'grandtotal',
		'bengkel_id'
	];

	public function bengkel()
	{
		return $this->belongsTo(\App\Models\Bengkel::class);
	}

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class, 'users_id');
	}

	public function d_orders()
	{
		return $this->hasMany(\App\Models\DOrder::class);
	}
}
